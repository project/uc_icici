
Pre-requistes
-------------

1. PHP version >= 5.0 (recommended version is >=   5.2).

2. JDK 1.4.2 or greater should be available (pre requisite for JavaBridge). 

3. PHP-java bridge, download from here:                                                                                     
   http://php-java-bridge.sourceforge.net/pjb/installation.php.

4. To configure PHP-java bridge, java and SFA jar files,  please read the SFA guide provided by the Payseal.          


Installation
------------

1. Place these files inside modules/uc_icici directory:
		uc_icici.info
    uc_icici.module

2. Copy the "Sfa" folder (PHP library provided by the ICICI) in modules/uc_icici/.

3. Copy the following PHP-Java-bridge configuration files in modules/uc_icici/Sfa/java:
    Java.inc
  	Java.php
    JavaProxy.php

4. Enable the payseal-ICICI payment gateway module under administer >> modules.    

